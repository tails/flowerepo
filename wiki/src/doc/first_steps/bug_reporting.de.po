# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-05-09 10:11+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: ENCODING\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Report an error\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"In this documentation we use the term *bug* to refer to a software error."
msgstr ""

#. type: Plain text
msgid "Reporting bugs is a great way of helping us improving Tails."
msgstr ""

#. type: Plain text
msgid ""
"Remember that **the more effectively you report a bug**, the more likely we "
"are to fix it."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"already_known\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Check if the bug is already known\n"
msgstr ""

#. type: Plain text
msgid "Have a look at:"
msgstr ""

#. type: Bullet: '  - '
msgid "the [[list of known issues|support/known_issues]]"
msgstr ""

#. type: Bullet: '  - '
msgid "the [[!tails_redmine desc=\"list of things to do\"]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"the [list of things that will be fixed or improved in the next release]"
"(https://labs.riseup.net/code/projects/tails/issues?query_id=111)"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"useful_bug_report\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "How to write a useful bug report\n"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"The first aim of a bug report is to **tell the developers exactly how to "
"reproduce the failure**."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"If that is not possible, try to **describe what went wrong in detail**.  "
"Write down the error messages, especially if they have numbers."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Write **clearly and be precise**. Say what you mean, and make sure it cannot "
"be misinterpreted."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"Be ready to provide extra information if the developers need it. If they did "
"not need it, they would not be asking for it."
msgstr ""

#. type: Plain text
msgid ""
"You can also refer to the great [How to Report Bugs Effectively](http://www."
"chiark.greenend.org.uk/~sgtatham/bugs.html), by Simon Tatham."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"whisperback\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Use WhisperBack\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"**WhisperBack is an application written specifically to report bugs anonymously\n"
"from inside Tails. If you are not able to use WhisperBack, see the [[special\n"
"cases|bug_reporting#special_cases]].**\n"
msgstr ""

#. type: Plain text
msgid ""
"WhisperBack will help you fill-up a bug report, including relevant technical "
"details and send it to us encrypted and through Tor."
msgstr ""

#. type: Title -
#, no-wrap
msgid "Start WhisperBack\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"To start <span class=\"application\">WhisperBack</span>, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">System Tools</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">WhisperBack</span></span>.\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Write the report\n"
msgstr ""

#. type: Plain text
msgid "WhisperBack lets you give plenty of useful information about your bug:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"**Summary** a summary of the bug, try to be short, clear and informative"
msgstr ""

#. type: Bullet: '  - '
msgid "**Name of the affected software**"
msgstr ""

#. type: Bullet: '  - '
msgid "**Exact steps to reproduce the error**"
msgstr ""

#. type: Bullet: '  - '
msgid "**Actual result and description of the error**"
msgstr ""

#. type: Bullet: '  - '
msgid "**Desired result**"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"You can also have a look at the <span class=\"guilabel\">technical details\n"
"to include</span> in your bug report. It will give us information about\n"
"your hardware, your version of Tails and the startup process.\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Optional email address\n"
msgstr ""

#. type: Plain text
msgid ""
"Giving us an email address allows us to contact you to clarify the problem. "
"This is needed for the vast majority of the reports we receive as most "
"reports without any contact information are useless. On the other hand it "
"also provides an opportunity for eavesdroppers, like your email or Internet "
"provider, to confirm that you are using Tails."
msgstr ""

#. type: Title -
#, no-wrap
msgid "Optional OpenPGP key\n"
msgstr ""

#. type: Plain text
msgid ""
"You can also indicate an OpenPGP key corresponding to this email address. "
"You can either give:"
msgstr ""

#. type: Bullet: '  - '
msgid "a **key ID**, if the key is available on public key servers"
msgstr ""

#. type: Bullet: '  - '
msgid "a **link to the key**, if the key is available on the web"
msgstr ""

#. type: Bullet: '  - '
msgid "a **public key block**, if the key is not publicly available"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Send your report\n"
msgstr ""

#. type: Plain text
msgid ""
"Once you are done writing your report, send it by clicking the *Send* button."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Once your email has been sent correctly you will get the following\n"
"notification: <span class=\"guilabel\">Your message has been sent</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"special_cases\"></a>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Special cases\n"
msgstr ""

#. type: Plain text
msgid ""
"You might not always be able to use WhisperBack. In those cases, you can "
"also send your bug report by [[email|support/talk]] directly."
msgstr ""

#. type: Plain text
msgid ""
"Note that if you send the report yourself, it might not be anonymous unless "
"you take special care (e.g. using Tor with a throw-away email account)."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a id=\"no_internet_access\"></a>\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "No internet access\n"
msgstr ""

#. type: Plain text
msgid "WhisperBack won't be able to send your bug report."
msgstr ""

#. type: Plain text
msgid "The following steps can be used as an alternative method:"
msgstr ""

#. type: Bullet: '1. '
msgid "In Tails, start WhisperBack"
msgstr ""

#. type: Bullet: '2. '
msgid "In the bug report window, expand \"technical details to include\""
msgstr ""

#. type: Bullet: '3. '
msgid "Copy everything in the \"debugging info\" box"
msgstr ""

#. type: Bullet: '4. '
msgid "Paste it to another document (using gedit for instance)"
msgstr ""

#. type: Bullet: '5. '
msgid "Save the document on a USB stick"
msgstr ""

#. type: Bullet: '6. '
msgid "Boot into a system with Internet connection and send your report"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Tails does not start\n"
msgstr ""

#. type: Plain text
msgid "See [[Tails_does_not_start]]."
msgstr ""
